# AWS OpsWorks

## Attach existing instance to AWS OpsWorks
To attach an instance to OpsWorks you need to provide permissions to the instance either by using Role or
User credentials who have the following permission on high level.
DescribeInstance
AWSOpsWorkFullAccess
CreateGroup

### Execute register command on instance
aws opsworks register --infrastructure-class ec2 --region us-east-1 --stack-id [stack_id] --ssh-username [username] --ssh-private-key [key-path] [instance_id]


## Steps to install chef on localhost
wget https://packages.chef.io/files/stable/chefdk/2.5.3/el/7/chefdk-2.5.3-1.el7.x86_64.rpm
sudo rpm -ivh chefdk-2.5.3-1.el7.x86_64.rpm
sudo mkdir -p /opt/chefdk/chefdir/cookbooks

### Create cookbook structure
chef generate cookbook [cookname]

### Execute cookbook on local machine
chef-client -z -o [cookname]




